# custom config lines
pull-filter ignore "dhcp-option DNS"
pull-filter ignore "dhcp-option DNS6"
dhcp-option DNS <your preferred DNS server>
dhcp-option DISABLE-NBT
block-outside-dns
register-dns