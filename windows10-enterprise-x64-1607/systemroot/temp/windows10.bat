:: windows 10 1607 post install script

bcdedit.exe /set bootmenupolicy legacy
bcdedit.exe /set bootstatuspolicy displayallfailures
bcdedit.exe /set disabledynamictick yes
bcdedit.exe /set quietboot off
bcdedit.exe /set nx AlwaysOff

reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Shell Extensions\Blocked" /f
reg add "HKLM\Software\Microsoft\Windows\CurrentVersion\Shell Extensions\Blocked" /v {1d27f844-3a1f-4410-85ac-14651078412d} /t REG_SZ /f
reg add HKLM\SYSTEM\ControlSet001\Control\Class\{4d36e968-e325-11ce-bfc1-08002be10318}\0000\ /v EnableUlps /t REG_DWORD /d 0x0 /f

mkdir C:\Windows\System32\GroupPolicy\Machine
mkdir C:\Windows\System32\GroupPolicy\User
mkdir C:\Windows\PolicyDefinitions

xcopy c:\temp\admx\* C:\Windows\PolicyDefinitions\ /E /F /H /R /Y
xcopy c:\temp\GroupPolicy\gpt.ini C:\Windows\System32\GroupPolicy\gpt.ini* /F /H /R /Y
xcopy c:\temp\GroupPolicy\Machine\Registry.pol C:\Windows\System32\GroupPolicy\Machine\Registry.pol* /F /H /R /Y
xcopy c:\temp\GroupPolicy\User\Registry.pol C:\Windows\System32\GroupPolicy\User\Registry.pol* /F /H /R /Y

powercfg.exe /HIBERNATE off

gpupdate /force

sc stop ClipSVC
sc stop MpsSvc
sc stop DiagTrack
sc stop SysMain
sc stop DPS
sc stop MapsBroker
sc stop PcaSvc
sc stop AppXSvc
sc stop WdiServiceHost
sc stop WdiSystemHost
sc stop WdNisSvc
sc stop WinDefend
sc stop lfsvc
sc stop wlidsvc
sc stop WerSvc
sc stop WbioSrvc
sc stop Sense
sc stop WlanSvc
sc stop SSDPSRV
sc stop fdPHost
sc stop FDResPub
sc stop iphlpsvc
sc stop WinHttpAutoProxySvc

sc config DPS start=disabled
sc config WdiServiceHost start=disabled
sc config WdiSystemHost start=disabled
sc config wuauserv start=disabled
sc config WpnService start=disabled
sc config WlanSvc start=disabled
sc config SSDPSRV start=disabled
sc config fdPHost start=disabled
sc config FDResPub start=disabled
sc config iphlpsvc start=disabled
sc config WinHttpAutoProxySvc start=disabled

reg add hklm\system\currentcontrolset\services\ClipSVC /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\MpsSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\DiagTrack /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\SysMain /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\MapsBroker /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\PcaSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\AppXSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\WdNisSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\WinDefend /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\lfsvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\wlidsvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\WerSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\WbioSrvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\Sense /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\UnistoreSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\UserDataSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\CDPSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\CDPUserSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\PimIndexMaintenanceSvc /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\MessagingService /f /v Start /t REG_DWORD /d 0x4
reg add hklm\system\currentcontrolset\services\WpnUserService /f /v Start /t REG_DWORD /d 0x4

schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\WindowsUpdate\Automatic App Update"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\WindowsUpdate\Scheduled Start"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\WindowsUpdate\sih"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\WindowsUpdate\sihboot"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\WDI\ResolutionHost"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\UpdateOrchestrator\Refresh Settings"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\UpdateOrchestrator\Schedule Scan"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\SystemRestore\SR"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Defrag\ScheduledDefrag"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Sysmain\ResPriStaticDbSync"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Sysmain\WsSwapAssessmentTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Autochk\Proxy"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Application Experience\StartupAppTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\AppID\SmartScreenSpecific"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Power Efficiency Diagnostics\AnalyzeSystem"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\SettingSync\BackupTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\SettingSync\NetworkStateChangeTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Shell\FamilySafetyRefreshTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\Shell\FamilySafetyMonitor"
schtasks.exe /Change /DISABLE /TN "Microsoft\Windows\DiskCleanup\SilentCleanup"
schtasks.exe /Change /DISABLE /TN "Microsoft\XblGameSave\XblGameSaveTask"
schtasks.exe /Change /DISABLE /TN "Microsoft\XblGameSave\XblGameSaveTaskLogon"
