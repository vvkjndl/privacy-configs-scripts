# privacy configs scripts

License for files residing in ***windows10-enterprise-x64-1607/systemroot/temp/admx/\****

> PLEASE NOTE: Microsoft Corporation (or based on where you live, one of its affiliates) licenses this supplement to you. You may use it with each validly licensed copy of Microsoft Office software (the “software”). You may not use the supplement if you do not have a license for the software. The license terms for the software apply to your use of this supplement. Microsoft provides support services for the supplement as described at www.support.microsoft.com/common/international.aspx. 

**Disclaimer for using windows10-enterprise-x64-1607 scripts and group policies**


> Only use if you fully understand the consequences, know what exactly you are doing and ready to hold yourself responsible in case something goes wrong. The assumption is that you do and must excercise full control over the system. Your system will be exposed to security risks. You must fully control what goes in, what goes out and what gets installed. Under any circumstances you will be responsible for any outcomes, positive or negative.

> It blocks/disables important updates, security software, built-in firewall and other critical components of Windows and Office; designed to protect users and provide better experience.

> The group policy templates included here are for Office 2016.

> READ the script carefully to see what it does and comment out what you do not need.